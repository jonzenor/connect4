# Jon Zenor's Submission #

## Design decisions ##
Due to work being extremely busy this week and having to work the weekend I did not have the time to write this completely from scratch with only my own objects.

For this reason, I decided to use the Laravel framework to give me a boost in productivity, by being able to leverage its built in security and session handling drivers, and the front end Blade template system.

For front end design, I used Tailwind CSS, and Daisy UI, which is a set of themes that are based on Tailwind CSS.

## How to Start ##
* git clone the project
* composer install
* php -S localhost:8000 -t public
* Open a browser to http://localhost:8000 

## Files to look at ##
These are the only files that I have the code used to develop this application:

* routes/web.php
* app/Http/Controllers/GameController.php
* app/Http/Player.php
* resources/views/thegame.blade.php
* tests/Feature/GameTest.php

## Questions? ##
If you have any questions, please send them to JLZenor@hey.com