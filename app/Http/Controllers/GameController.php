<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Player;

class GameController extends Controller
{
    private $columns = 6;
    private $rows = 7;
    
    private $Player;

    public function __construct()
    {
        $this->Player = new \App\Http\Player();        
    }

    public function index()
    {
        $board = $this->getBoardStatus();

        return view('thegame', [
            'turn' => $this->Player->turn(),
            'player' => $this->Player->color(),
            'board' => $board,
        ]);
    }

    public function start()
    {
        $this->Player->newGame();

        $this->resetTheBoard();

        return redirect()->route('home');
    }

    public function reset()
    {
        $this->Player->reset();

        return redirect()->route('home');
    }

    public function again()
    {
        $firstPlayer = $this->Player->getLoser();

        $this->Player->reset();
        $this->resetTheBoard();

        $this->Player->newGame();
        $this->Player->setPlayer($firstPlayer);

        return redirect()->route('home');
    }

    public function play($column = 0)
    {
        if (!$this->Player->getTurn()) {
            return redirect()->route('home')->withErrors('game-not-started');
        }

        if (!is_numeric($column) || $column > $this->columns || $column < 1) {
            return redirect()->route('home')->withErrors('invalid-column');
        }

        // Find out the first available row in this column
        $firstAvailable = 1;
        $board = $this->getBoardStatus();

        // Make sure this column is not full
        if ($board[$column][$this->rows] != 0) {
            return redirect()->route('home')->withErrors('invalid-column');
        }

        // Loop through the rows in this column to see what the first available row is
        for ($row = 1; $row <= $this->rows; $row ++) {
            if ($board[$column][$row] == 0) {
                $firstAvailable = $row;
                
                // Break out of the for loop
                break;
            }
        }

        Session::put($column . '-' . $firstAvailable, $this->Player->getPlayer());

        $winner = $this->checkForWinner($column, $firstAvailable);
        
        if ($winner) {
            $this->Player->setWinner($winner);
            $this->Player->setTurn(0);
            return redirect()->route('home');
        }

        $this->Player->alternatePlayer();

        return redirect()->route('home');
    }

    private function getBoardStatus()
    {
        $board = array();

        for ($column = 1; $column <= $this->columns; $column ++) {
            for ($row = 1; $row <= $this->rows; $row ++) {
                $board[$column][$row] = Session::get($column . '-' . $row);
            }
        }

        return $board;
    }

    private function resetTheBoard()
    {
        for ($column = 1; $column <= $this->columns; $column ++) {
            for ($row = 1; $row <= $this->rows; $row ++) {
                Session::put($column . '-' . $row, '0');
            }
        }
    }

    private function checkForWinner($placedColumn, $placedRow)
    {
        $board = $this->getBoardStatus();

        $check = array();
        $check['player'] = 'nobody';

        // First check the rows
        for ($row = 1; $row <= $this->rows; $row ++) {
            for ($column = 1; $column <= $this->columns; $column ++) {

                // First see if there is a piece here
                if ($board[$column][$row]) {

                    // Now check if the color of piece  here matches the previous player
                    if ($board[$column][$row] == $check['player'])
                    {
                        // The current piece is owned by the player we're tracking, so increment it
                        $check['count'] ++;

                        if ($check['count'] == 4) {
                            return $check['player'];
                        }
                    } else {
                        // The current piece is owned by a different player, so restart the count
                        $check['player'] = $board[$column][$row];
                        $check['count'] = 1;
                    }
                } else {
                    $check['player'] = 'nobody';
                    $check['count'] = 0;
                }
            }
        }

        // Now check the columns
        for ($column = 1; $column <= $this->columns; $column ++) {
            for ($row = 1; $row <= $this->rows; $row ++) {

                // First see if there is a piece here
                if ($board[$column][$row]) {

                    // Now check if the color of piece  here matches the previous player
                    if ($board[$column][$row] == $check['player'])
                    {
                        // The current piece is owned by the player we're tracking, so increment it
                        $check['count'] ++;

                        if ($check['count'] == 4) {
                            return $check['player'];
                        }
                    } else {
                        // The current piece is owned by a different player, so restart the count
                        $check['player'] = $board[$column][$row];
                        $check['count'] = 1;
                    }
                } else {
                    $check['player'] = 'nobody';
                    $check['count'] = 0;
                }
            }
        }

        // Now check the diagonal
        // We should only have a few spaces to check if we focus on only the piece that was placed
        $count = 1;
        $currentPlayer = Session::get('player');
        
        $column = $placedColumn;
        $row = $placedRow;

        for ($i = 1; $i <= 3; $i ++) {
            if ((($column - $i) < 1) || (($row - $i) < 1)) {
                // We reached the bottom/left side of the board
                break;
            } elseif ($board[$column - $i][$row - $i] == $currentPlayer) {
                $count ++;
            } else {
                // This piece belonged to someone else, or is blank, so end the count
                break;
            }
        }

        for ($i = 1; $i <= 3; $i ++) {
            if ((($column + $i) > $this->columns) || (($row + $i) > $this->rows)) {
                // We reached the top/right side of the board
                break;
            } elseif ($board[$column + $i][$row + $i] == $currentPlayer) {
                $count ++;
            } else {
                // This piece belonged to someone else, or is blank, so end the count
                break;
            }
        }

        if ($count >= 4) {
            return $currentPlayer;
        }

        // Now check the other diagonal
        // We should only have a few spaces to check if we focus on only the piece that was placed
        $count = 1;
        $currentPlayer = $this->Player->getPlayer();
        
        $column = $placedColumn;
        $row = $placedRow;

        for ($i = 1; $i <= 3; $i ++) {
            if ((($column - $i) < 1) || (($row + $i) > $this->rows)) {
                // We reached the bottom/left side of the board
                break;
            } elseif ($board[$column - $i][$row + $i] == $currentPlayer) {
                $count ++;
            } else {
                // This piece belonged to someone else, or is blank, so end the count
                break;
            }
        }

        for ($i = 1; $i <= 3; $i ++) {
            if ((($column + $i) > $this->columns) || (($row - $i) < 1)) {
                // We reached the top/right side of the board
                break;
            } elseif ($board[$column + $i][$row - $i] == $currentPlayer) {
                $count ++;
            } else {
                // This piece belonged to someone else, or is blank, so end the count
                break;
            }
        }

        if ($count >= 4) {
            return $currentPlayer;
        }

        return null;
    }
}
