<?php

namespace App\Http;

use Illuminate\Support\Facades\Session;

class Player
{
    private $current = null;
    private $turn = 0;
    private $winner = null;

    function __construct()
    {
        $this->current = Session::get('player');
        $this->turn = Session::get('turn');
        $this->winner = Session::get('winner');
    }

    public function turn()
    {
        return $this->turn;
    }

    public function color()
    {
        return $this->current;
    }

    public function newGame()
    {
        $this->setPlayer($this->pickStartingPlayer());
        $this->setTurn('1');
    }

    public function reset()
    {
        Session::flush();
    }

    public function getPlayer()
    {
        return $this->current;
    }

    public function getWinner()
    {
        return $this->winner;
    }

    public function getLoser()
    {
        if ($this->winner == null) {
            return null;
        }

        return ($this->getWinner() == 'red') ? 'yellow' : 'red';
    }

    public function getTurn()
    {
        return $this->turn;
    }

    public function setPlayer($value)
    {
        $this->current = $value;
        Session::put('player', $value);
    }

    public function setTurn($value)
    {
        $this->turn = $value;
        Session::put('turn', $value);
    }

    public function setWinner($value)
    {
        $this->winner = $value;
        Session::put('winner', $value);
    }

    private function pickStartingPlayer()
    {
        $number = rand(1, 2);

        if ($number == '1') {
            return 'red';
        }

        return 'yellow';
    }

    public function alternatePlayer()
    {
        $nextPlayer = ($this->getPlayer() == 'red') ? 'yellow' : 'red';
        $this->setPlayer($nextPlayer);
    }

}