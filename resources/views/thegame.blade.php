<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="bg-base-100">
    <head>
        <title>Focus on the Connect 4 Family</title>

        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <html data-theme="forest">

    </head>
    <body>
        <div class="content-center w-10/12 p-5 mx-auto my-5 text-2xl text-center rounded-md bg-neutral text-base-content">
            <a href="{{ route('home') }}">Focus on the Connect 4 Family</a>
        </div>

        @if (Session::get('winner'))
            <div class="content-center w-full mx-auto my-4 text-center alert alert-success sm:w-3/4 lg:w-1/3">
                {{ ucfirst(Session::get('winner')) }} Wins!
            </div>
        @endif

        @foreach ($errors->all() as $error)
            <div class="content-center w-full mx-auto my-4 text-center alert alert-error sm:w-3/4 lg:w-1/3">
                @if($error == 'invalid-column')
                    Invalid Column Selected
                @elseif ($error == 'game-not-started')
                    Game has not yet begun
                @else
                    There was an unknown error
                @endif
            </div>
        @endforeach
      
        <div id="ControlPanel" class="w-full p-3 mx-auto text-center bg-base-300 sm:w-3/4 lg:w-1/3">
            @if ($player)
                <span class="card-title">Player:</span> <span class="badge mx-2 text-xl @if($player == 'red') badge-error @else badge-warning @endif">{{ ucfirst($player) }}</span>
            @endif
        </div>

        <div id="MainBoard" class="w-full p-3 mx-auto text-center bg-base-300 sm:w-3/4 lg:w-1/3">
            @for ($column = 1; $column <= 6; $column++)
                <div id="column-{{ $column }}" class="inline-block mx-auto">

                    @for ($row = 7; $row >= 1; $row--)
                        <div class="p-3 font-bold border border-gray-700 w-1/8">
                            @if ($board[$column][$row] == 'red')
                                <span class="text-2xl badge badge-error">&#8224;</span>
                            @elseif ($board[$column][$row] == 'yellow')
                                <span class="text-2xl badge badge-warning">&#8224;</span>
                            @else
                                <span class="text-2xl badge badge-ghost">&nbsp;</span>
                            @endif
                        </div>
                    @endfor

                    <div class="p-3 font-bold border border-gray-700 w-1/8">
                        @if($turn)
                            <a href="{{ route('play', $column) }}" class="btn @if($board[$column][7] != 0) btn-disabled @else btn-primary @endif">{{ $column }}</a>
                        @endif
                    </div>

                </div>
            @endfor

        </div>

        <div id="ControlPanel" class="w-full p-3 mx-auto text-center bg-base-300 sm:w-3/4 lg:w-1/3">
            @if ($turn)                    
                Choose a column to place your piece in.
            @endif
        </div>

        <div id="ControlPanel" class="w-full p-3 mx-auto text-center bg-base-300 sm:w-3/4 lg:w-1/3">
            @if (Session::get('winner'))
                <a href="{{ route('again') }}" class="btn btn-primary">Play Again?</a>
            @elseif ($turn <= 0)
                <a href="{{ route('start') }}" class="btn btn-primary">Start Game</a>
            @else
                <a href="{{ route('reset') }}" class="btn btn-accent">Reset Game</a>
            @endif
        </div>

    </body>
</html>