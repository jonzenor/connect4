<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Session;
use Tests\TestCase;

class GameTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    // DONE: Create the main game page
    /** @test */
    public function the_main_page_loads()
    {
        $response = $this->get('/');

        $response->assertViewIs('thegame');
    }

    // DONE: Load the connect 4 board
    /** @test */
    public function the_game_board_loads_on_the_main_page()
    {
        $response = $this->get('/');

        $response->assertSee('column-1');
    }

    // DONE: Create a "New Game" button
    /** @test */
    public function the_start_game_button_shows_up_on_the_main_page()
    {
        $response = $this->get(route('home'));

        $response->assertSee(route('start'));
    }

    /** @test */
    public function the_start_game_function_redirects_back_to_home()
    {
        $response = $this->get(route('start'));

        $response->assertRedirect(route('home'));
    }

    /** @test */
    public function the_start_game_function_sets_up_session_data()
    {
        $response = $this->get(route('start'));

        $response->assertSessionHas('player');
        $this->assertSame(Session::get('turn'), '1');
    }

    /** @test */
    public function after_starting_the_start_button_hides()
    {
        $this->get(route('start'));
        $response = $this->get(route('home'));

        $response->assertDontSee(route('start'));
    }
    
    // DONE: Choose a random player to start first
    /** @test */
    public function ensure_that_after_starting_a_random_player_is_chosen_to_go_first()
    {
        // In ten attempts at starting the game, we should see both starting positions
        $startingPlayer = array();

        for ($i = 0; $i < 10; $i++) {
            $this->get(route('start'));
            $startingPlayer[] = Session::get('player');
        }

        $this->assertContains('yellow', $startingPlayer);
        $this->assertContains('red', $startingPlayer);
    }

    /** @test */
    public function make_sure_the_board_shows_whose_turn_it_is()
    {
        $this->get(route('start'));
        $response = $this->get(route('home'));

        if (Session::get('player') == 'red') {
            $response->assertSeeInOrder(['Player:', 'Red']);
        } else {
            $response->assertSeeInOrder(['Player:', 'Yellow']);
        }
    }

    // DONE: Allow the user to choose a column to place their piece in
    /** @test */
    public function make_sure_the_column_selection_buttons_show_up()
    {
        $this->get(route('start'));
        $response = $this->get(route('home'));

        for ($i = 1; $i < 6; $i ++) {
            $response->assertSee(route('play', $i));
        }
    }

    /** @test */
    public function the_play_buttons_redirect_back_to_home()
    {
        $response = $this->get(route('play', 2));

        $response->assertRedirect(route('home'));
    }

    /** @test */
    public function the_play_button_places_a_piece_in_storage()
    {
        $this->get(route('start'));
        
        // We have to save the current player because after we play a piece the player changes
        $player = Session::get('player');

        $this->get(route('play', 2));

        $this->assertEquals(Session::get('2-1'), $player);
    }

    /** @test */
    public function pieces_show_on_the_board()
    {
        $this->get(route('start'));
        $this->get(route('play', 3));
        $response = $this->get(route('home'));

        $response->assertSeeInOrder(['column-3', '&#8224;', 'column-4'], false);
    }    

    // DONE: Make sure the piece always falls to the bottom of the column
    /** @test */
    public function the_play_button_places_a_piece_in_the_correct_row()
    {
        $this->get(route('start'));

        $player = Session::get('player');

        // Place a piece in row 1 of column 2
        Session::put('2-1', 'yellow');

        $this->get(route('play', 2));

        // Make sure the piece got put in row 2
        $this->assertEquals(Session::get('2-2'), $player);
    }

    // DONE: Switch what player plays after the move
    /** @test */
    public function the_player_switches_after_playing_a_piece()
    {
        $this->get(route('start'));
        $startingPlayer = Session::get('player');

        $this->get(route('play', 6));
        $this->assertNotEquals(Session::get('player'), $startingPlayer);
    }

    // DONE: Validate input to keep bad actors from breaking things
    /** 
     * @test
     * @dataProvider playInputValidation
     * */
    public function make_sure_users_cannot_enter_invalid_data($invalidInput)
    {
        $this->get(route('start'));
        $response = $this->get(route('play', $invalidInput));

        $response->assertSessionHasErrors();
    }

    // DONE: Make the button go inactive when the top of that column has been reached
    /** @test */
    public function disable_buttons_when_column_is_full()
    {
        $this->get(route('start'));
        
        // Place a piece in the last column
        Session::put('1-7', 'yellow');
        $response = $this->get(route('home'));

        $response->assertSeeInOrder([route('play', 1), "disabled", "</a>"], false);
    }

    /** @test */
    public function give_an_error_if_attempting_to_use_a_column_that_is_full()
    {
        $this->get(route('start'));
        
        // Place a piece in the last column
        Session::put('1-7', 'yellow');
        $response = $this->get(route('play', 1));

        $response->assertSessionHasErrors();
    }

    // DONE: Provide feedback for errors
    /** @test */
    public function playing_the_wrong_column_shows_an_error()
    {
        $this->get(route('start'));
        $response = $this->followingRedirects()->get(route('play', 99));

        $response->assertSee('Invalid Column Selected');
    }

    /** @test */
    public function playing_a_correct_column_shows_no_error()
    {
        $this->get(route('start'));
        $response = $this->followingRedirects()->get(route('play', 4));

        $response->assertDontSee('Invalid Column Selected');
    }

    // DONE: Check that the game has started
    /** @test */
    public function ensure_the_column_buttons_are_hiding_if_the_game_has_not_started()
    {
        $response = $this->get(route('home'));

        $response->assertDontSee(route('play', 1));
    }

    /** @test */
    public function make_sure_the_game_was_started_before_attempting_to_play()
    {
        $response = $this->get(route('play', 2));

        $response->assertSessionHasErrors();
    }

    /** @test */
    public function if_the_game_has_not_started_show_the_user_the_error()
    {
        $response = $this->followingRedirects()->get(route('play', 2));

        $response->assertSee('Game has not yet begun');
    }

    /** @test */
    public function show_a_reset_link_if_the_game_has_started()
    {
        $this->get(route('start'));
        $response = $this->get(route('home'));

        $response->assertSee(route('reset'));
    }

    /** @test */
    public function reset_link_resets_the_game()
    {
        $this->get(route('start'));
        $this->get(route('reset'));

        $this->assertTrue(!Session::get('turn'));
    }

    /** @test */
    public function reset_link_redirects_to_home()
    {
        $response = $this->get(route('reset'));

        $response->assertRedirect(route('home'));
    }

    // DONE: Check for a winning condition
    /** @test */
    public function verify_a_win_across_a_row()
    {
        $this->get(route('start'));

        Session::put('1-1', 'red');
        Session::put('2-1', 'red');
        Session::put('3-1', 'red');

        // Make sure it's red's turn
        Session::put('player', 'red');

        // Placing a piece should declare a victory
        $response = $this->followingRedirects()->get(route('play', 4));
        
        $response->assertSee('Red Wins!');
    }

    /** @test */
    public function winning_turns_off_the_column_buttons()
    {
        $this->get(route('start'));

        Session::put('1-1', 'red');
        Session::put('2-1', 'red');
        Session::put('3-1', 'red');

        // Make sure it's red's turn
        Session::put('player', 'red');

        // Placing a piece should declare a victory
        $response = $this->followingRedirects()->get(route('play', 4));
        
        $response->assertDontSee(route('play', 1));
    }

    /** @test */
    public function verify_a_win_down_a_column()
    {
        $this->get(route('start'));

        Session::put('1-1', 'yellow');
        Session::put('1-2', 'yellow');
        Session::put('1-3', 'yellow');

        // Make sure it's yellow's turn
        Session::put('player', 'yellow');

        // Placing a piece should declare a victory
        $response = $this->followingRedirects()->get(route('play', 1));
        
        $response->assertSee('Yellow Wins!');
    }

    /** @test */
    public function verify_a_diagonal_win_going_up()
    {
        $this->get(route('start'));

        Session::put('4-4', 'yellow');
        Session::put('3-3', 'yellow');
        Session::put('2-2', 'yellow');

        // Make sure it's yellow's turn
        Session::put('player', 'yellow');

        // Placing a piece should declare a victory
        $response = $this->followingRedirects()->get(route('play', 1));
        
        $response->assertSee('Yellow Wins!');
    }

    /** @test */
    public function verify_a_diagonal_win_going_down()
    {
        $this->get(route('start'));

        Session::put('4-2', 'yellow');
        Session::put('3-3', 'yellow');
        Session::put('2-4', 'yellow');

        // Make sure it's yellow's turn
        Session::put('player', 'yellow');

        // Placing a piece should declare a victory
        $response = $this->followingRedirects()->get(route('play', 5));
        
        $response->assertSee('Yellow Wins!');
    }    

    // DONE: Ask the player if they want to play again, and start with the looser
    /** @test */
    public function after_a_win_show_the_play_again_button()
    {
        $this->get(route('start'));

        Session::put('1-1', 'red');
        Session::put('2-1', 'red');
        Session::put('3-1', 'red');

        // Make sure it's red's turn
        Session::put('player', 'red');

        // Placing a piece should declare a victory
        $response = $this->followingRedirects()->get(route('play', 4));
        
        $response->assertSee(route('again'));
    }

    /** @test */
    public function the_play_again_button_works()
    {
        $this->get(route('start'));
        Session::put('1-3', 'red');

        $this->get(route('again'));

        $this->assertEquals(Session::get('1-3'), '0');
    }

    /** @test */
    public function the_play_again_button_redirects_home()
    {
        $response = $this->get(route('again'));

        $response->assertRedirect(route('home'));
    }

    /** @test */
    public function the_play_again_button_starts_with_the_looser()
    {
        $this->get(route('start'));
        Session::put('player', 'yellow');
        Session::put('winner', 'yellow');

        $this->get(route('again'));
        $this->assertEquals(Session::get('player'), 'red');
    }

    // TODO: Increment the turn count just for stat tracking

    public function playInputValidation()
    {
        return [
            ['asdf'],
            ['100'],
            ['-144'],
            ['0'],
            [''],
        ];
    }
}
